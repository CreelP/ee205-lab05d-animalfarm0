
/////////////////////////////////////////////////////////////////////////////
////////////
///////////// University of Hawaii, College of Engineering
///////////// @brief Lab 05b - Animalfarm0 - EE 205 - Spr 2022
/////////////
///////////// @file reportCats.c
///////////// @version 1.0
/////////////
///////////// @author Creel Patrocinio <creel@hawaii.edu>
///////////// @date 02/20/2022
///////////////////////////////////////////////////////////////////////////////////////
///////



#include "reportCats.h"


void printCat(const size_t index) {
   if (index > numberOfCats) {
      printf("animalfarm0: Bad cat [%ld]\n", index);

   }
   
   else {
      printf("cat index = [%lu]name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%f]\n", index,
            catName[index], catGender[index], catBreed[index], catIsFixed[index], catWeight[index]);           

   }

}

void printAllCats() {
   for (size_t index = 0; index < numberOfCats; ++index) {
      printCat(index);

   }
}


int findCat(const char *name) {
   for (size_t i = 0; i < numberOfCats; ++i) {
      if (strcmp(name, catName[i]) == 0) {
         return i;
   
      }

   }
   printf("Error: Cat not in the database %s\n", index);
   return -1;

}





