////////////
///////////////// University of Hawaii, College of Engineering
///////////////// @brief Lab 05b - Animalfarm0 - EE 205 - Spr 2022
/////////////////
///////////////// @file updateCats.h
///////////////// @version 1.0
/////////////////
///////////////// @author Creel Patrocinio <creel@hawaii.edu>
///////////////// @date 02/20/2022
///////////////////////////////////////////////////////////////////////////////////////////
///////////
//
#pragma once

#include "catDatabase.h"
#include "addCats.h"
#include <string.h>

void updateCatName(const size_t index, const char *newName);
void fixCat(const size_t index);
void updateCatWeight(const size_t index, const float weight);


