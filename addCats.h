/////////////////////////////////////////////////////////////////////////////
////////////
///////////// University of Hawaii, College of Engineering
///////////// @brief Lab 05b - Animalfarm0 - EE 205 - Spr 2022
/////////////
///////////// @file addCats.h
///////////// @version 1.0
/////////////
///////////// @author Creel Patrocinio <creel@hawaii.edu>
///////////// @date 02/20/2022
///////////////////////////////////////////////////////////////////////////////////////
////////
//
#pragma once

#include "catDatabase.h"

int addCat(const char *name,
            const enum Gender gender,
            const enum Breed breed,
            const bool isFixed,
            const float weight);

bool validName(const char *name);
bool validWeight(const float weight);



