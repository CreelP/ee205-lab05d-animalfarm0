/////////////////////////////////////////////////////////////////////////////
//////
/////// University of Hawaii, College of Engineering
/////// @brief Lab 05b - Animalfarm0 - EE 205 - Spr 2022
///////
/////// @file catDatabase.c
/////// @version 1.0
///////
/////// @author Creel Patrocinio <creel@hawaii.edu>
/////// @date 02/20/2022
/////////////////////////////////////////////////////////////////////////////////
//


#include "catDatabase.h"

size_t numberOfCats;
char catName[MAX_CAT_NAME][MAX_CAT];
enum Gender catGender[MAX_CAT];
enum Breed catBreed[MAX_CAT];
bool catIsFixed[MAX_CAT];
float catWeight[MAX_CAT];



void initializeCatDatabase() {  
   numberOfCats = 0;
   memset(catName, '\0', sizeof catName);
   memset(catGender, UNKNOWN_GENDER, sizeof catGender);
   memset(catBreed, UNKNOWN_BREED, sizeof catBreed);
   memset(catIsFixed, false, sizeof catIsFixed); // Assume cat is not fixed
   memset(catWeight, 0.0, sizeof catWeight);

}





