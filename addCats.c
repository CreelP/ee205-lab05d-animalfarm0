/////////////////////////////////////////////////////////////////////////////
//////////
/////////// University of Hawaii, College of Engineering
/////////// @brief Lab 05b - Animalfarm0 - EE 205 - Spr 2022
///////////
/////////// @file addCats.c
/////////// @version 1.0
///////////
/////////// @author Creel Patrocinio <creel@hawaii.edu>
/////////// @date 02/20/2022
/////////////////////////////////////////////////////////////////////////////////////
//////

#include "addCats.h"



int addCats(const char *name, 
            const enum Gender gender, 
            const enum Breed breed,
            const bool isFixed,
            const float weight) {

         

         if (numberOfCats < MAX_CAT && 
             validName(name) == true &&
             validWeight(weight) == true) {

            strcpy(catName[numberOfCats], name);
            catGender[numberOfCats] = gender;
            catBreed[numberOfCats] = breed;
            catIsFixed[numberOfCats] = isFixed;
            catWeight[numberOfCats] = weight;
            numberOfCats++;
            return numberOfCats;


         }
   
         else {

            printf("Invalid addCat input\n");
            printf("DB has space: %s\nvalidName: %s\nvalidWeight: %s\n",
                  numberOfCats < MAX_CAT ? "true": "false",
                  validName(name) ? "true" : "false",
                  validWeight(weight) ? "true" : "false");

            return -1;

         }

}




bool validName(const char *name) {
   bool nameTaken = false;
   for (size_t i = 0; i < numberOfCats; ++i) {

      if (strcmp(catName[i], name) == 0) {
            
            printf("Name taken%s \n", name);
            nameTaken = true; 
            continue;
            
            }
      }


   

   if (nameTaken == false &&
      (strlen(name) > 0) &&
      (strlen(name) <= MAX_CAT_NAME -1)) {

         return true;

      }
   return false;

}

bool validWeight(const float weight) {
   if (weight > 0) {
      return true;

   }
   return false;


}





