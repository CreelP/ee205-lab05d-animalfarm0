###############################################################################
### University of Hawaii, College of Engineering
### @brief  Lab 01a - Hello World - EE 205 - Spr 2022
###
### @file MakefileCC = gcc
### @version 1.1
###
### Tells git which files to ignore
###
### @author Creel Patrocinio <creel@hawaii.edu>
### @date   2/21/2022
###############################################################################

CC = gcc
CFLAGS = -g -Wall -Wextra
TARGET = animalFarm

all: $(TARGET)

addCats.o: addCats.c addCats.h catDatabase.h
		$(CC) $(CFLAGS) -c addCats.c

catDatabase.o: catDatabase.c catDatabase.h
		$(CC) $(CFLAGS) -c catDatabase.c

deleteCats.o: deleteCats.c deleteCats.h catDatabase.h
		$(CC) $(CFLAGS) -c deleteCats.c

reportCats.o: reportCats.c reportCats.h catDatabase.h
		$(CC) $(CFLAGS) -c reportCats.c

updateCats.o: updateCats.c updateCats.h catDatabase.h addCats.h
		$(CC) $(CFLAGS) -c updateCats.c

main.o: main.c addCats.h catDatabase.h deleteCats.h reportCats.h updateCats.h
		$(CC) $(CFLAGS) -c main.c

animalFarm: main.o addCats.o catDatabase.o deleteCats.o reportCats.o updateCats.o
		$(CC) $(CFLAGS) -o $(TARGET) main.o addCats.o catDatabase.o deleteCats.o reportCats.o updateCats.o

clean:
		rm -f $(TARGET) *.o

test: animalFarm



