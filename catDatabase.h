/////////////////////////////////////////////////////////////////////////////
////////
///////// University of Hawaii, College of Engineering
///////// @brief Lab 05b - Animalfarm0 - EE 205 - Spr 2022
/////////
///////// @file catDatabase.h
///////// @version 1.0
/////////
///////// @author Creel Patrocinio <creel@hawaii.edu>
///////// @date 02/20/2022
///////////////////////////////////////////////////////////////////////////////////
////

#pragma once


#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#define MAX_CAT_NAME 30
#define MAX_CAT 10


extern size_t numberOfCats;

extern char catName[MAX_CAT][MAX_CAT_NAME];

// Gender
enum Gender {

   UNKNOWN_GENDER,
   MALE,
   FEMALE

};

extern enum Gender catGender[];

// Breed
enum Breed {

   UNKNOWN_BREED,
   MAINE_COON,
   MANX,
   SHORTHAIR,
   PERSIAN,
   SPHYNX

};

extern enum Breed catBreed[];

extern bool catIsFixed[MAX_CAT];
extern float catWeight[MAX_CAT];

void initializeCatDatabase();


