////////////
/////////////// University of Hawaii, College of Engineering
/////////////// @brief Lab 05b - Animalfarm0 - EE 205 - Spr 2022
///////////////
/////////////// @file updateCats.c
/////////////// @version 1.0
///////////////
/////////////// @author Creel Patrocinio <creel@hawaii.edu>
/////////////// @date 02/20/2022
/////////////////////////////////////////////////////////////////////////////////////////
/////////



#include "updateCats.h"
#include <string.h>



void updateCatName(const size_t index, const char *newName) {
   if (validName(newName) == true) {
      strcpy(catName[index], newName);
   }


}

void fixCat(const size_t index) {
   catIsFixed[index] = true;

}

void updateCatWeight(const size_t index, const float newWeight) {
   if (validWeight(newWeight) == true) {
      catWeight[index] = newWeight;
      
   }


}






